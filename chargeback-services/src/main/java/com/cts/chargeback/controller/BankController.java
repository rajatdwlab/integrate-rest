package com.cts.chargeback.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cts.chargeback.entity.ChargebackData;
import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.ProfitData;
import com.cts.chargeback.entity.StockPrice;
import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.AdminUsersAndFunds;
import com.cts.chargeback.objects.FundUser;
import com.cts.chargeback.objects.Login;
import com.cts.chargeback.objects.UserAndTransaction;
import com.cts.chargeback.service.BankService;
import com.cts.chargeback.service.UserService;

@CrossOrigin
@RestController("service")
public class BankController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private BankService bankService;
	
	@GetMapping("/run")
	public ResponseEntity<String> run() {
		return ResponseEntity.ok().body("Haha");
	}
	@GetMapping("/users")
	public ResponseEntity<List<User>> listUsers() {
		System.out.println("List user");
		List<User> users = userService.listUsers();
		return ResponseEntity.ok().body(users);
	}
	@GetMapping("/user/{userId}")
	public ResponseEntity<User> showUser(@PathVariable("userId") Integer userId) {
		User user = userService.showUser(userId);
		return ResponseEntity.ok().body(user);
	}
	@GetMapping("/usersIndividual")
	public ResponseEntity<List<User>> listIndividualUsers() {
		List<User> users = userService.listIndividualUsers();
		return ResponseEntity.ok().body(users);
	}
	
	@PostMapping("/addUser")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		System.out.println("--addUser API called");
		User userObj = userService.add(user);
		System.out.println("addUser:userId "+userObj.getUserId());
	    return ResponseEntity.ok().body(userObj);
	}
	@PostMapping("/loginSimple")
	public ResponseEntity<User> loginSimple(@RequestBody Login login) {
		User user=userService.validateUser(login);
		if(user==null){
			user=new User();
			user.setPassword(null);
		}
		return ResponseEntity.ok().body(user);
	}
	@PostMapping("/login")
	public ResponseEntity<UserAndTransaction> loginUser(@RequestBody Login login) {
		System.out.println("--login API called");
		User user=userService.validateUser(login);
		UserAndTransaction uat;
		if(user!=null){
			List<Transaction> transactionsList= bankService.getTransactions(Long.valueOf(user.getUserId()),"BUY");
			System.out.println(transactionsList.size()+" : transactionsList.size()");
			uat = new UserAndTransaction(user, transactionsList);
			
		}else{
			uat=new UserAndTransaction(null,null);
		}
		return ResponseEntity.ok().body(uat);
	}
	@PostMapping("/loginUser")
	public ResponseEntity<?> loginDiffUser(@RequestBody Login login) {
		System.out.println("--login API called");
		User user=userService.validateUser(login);
		if(user.getRole().equals("IndividualUser") || user.getRole().equals("FundUser")){
			List<Transaction> transactionsList= bankService.getTransactions(Long.valueOf(user.getUserId()),"BUY");
			System.out.println(transactionsList.size()+" : transactionsList.size()");
			UserAndTransaction uat = new UserAndTransaction(user, transactionsList);
			return ResponseEntity.ok().body(uat);
		}else{
			List<User> users = userService.listIndividualUsers();
			List<Fund> fundList = bankService.getFundList();
			AdminUsersAndFunds auaf= new AdminUsersAndFunds(user,users,fundList);
			return ResponseEntity.ok().body(auaf);
		}
	}
	
	@GetMapping("/transactions/{userId}/{buyOrSell}")
	public ResponseEntity<List<Transaction>> getTransactionsForBuyOrSell(@PathVariable("userId") long userId,@PathVariable("buyOrSell") String buyOrSell) {
		System.out.println("---- getTransactionsForBuyOrSell api called-- userId: " + userId);
		List<Transaction> transactionList = bankService.getTransactions(userId, buyOrSell);
		return ResponseEntity.ok().body(transactionList);
	}
	
	@GetMapping("/notSold/{userId}")
	public ResponseEntity<List<Transaction>> getTransactionsNotSold(@PathVariable("userId") long userId) {
		System.out.println("---- getTransactionsForBuyOrSell api called-- userId: " + userId);
		List<Transaction> transactionList = bankService.getTransactionsNotSold(userId);
		return ResponseEntity.ok().body(transactionList);
	}
	
	@PostMapping("/transactionEntry")
	public ResponseEntity<Transaction> makeTranasction(@RequestBody Transaction transaction) {
		System.out.println("---make Transaction");
		bankService.generateTransaction(transaction);
		System.out.println("---- Id is " + transaction.getTransactionId());
		return ResponseEntity.ok().body(transaction);
	}
	
	@PostMapping("/fund")
	public ResponseEntity<Fund> createFund(@RequestBody Fund fund) {
		System.out.println("---create Fund");
		bankService.createFund(fund);		
		return ResponseEntity.ok().body(fund);
	}
	
	@PostMapping("/addFundUser")
	public ResponseEntity<Fund> addUserInFund(@RequestBody FundUser fundUser) {
		System.out.println("--addUserFund");
		Fund fund=bankService.enrollUser(fundUser);		
		return ResponseEntity.ok().body(fund);
	}
	@GetMapping("/funds")
	public ResponseEntity<List<Fund>> getFundList() {
		System.out.println("--showAllFunds");
		List<Fund> fundList = bankService.getFundList();	
		return ResponseEntity.ok().body(fundList);
	}
	@GetMapping("/fundUsers/{fundId}")
	public ResponseEntity<List<User>> getFundUsers(@PathVariable("fundId") long fundId) {
		System.out.println("--showFundUsers");
		List<User> users = bankService.showFundUsers(fundId);	
		return ResponseEntity.ok().body(users);
	}
	@GetMapping("/stockPrice/{stockName}")
	public ResponseEntity<StockPrice> getStockPrice(@PathVariable("stockName") String stockName) {
		System.out.println("--showStockPrice for "+ stockName);
		StockPrice stockPrice = bankService.showStockPrice(stockName);
		return ResponseEntity.ok().body(stockPrice);
	}
	@GetMapping("/stockPrices")
	public ResponseEntity<List<StockPrice>> getStockPrices() {
		System.out.println("--showStockPrices");
		List<StockPrice> stockPriceList = bankService.showStockPrice();	
		return ResponseEntity.ok().body(stockPriceList);
	}
	@GetMapping("/chargeBackPercent")
	public ResponseEntity<List<ChargebackData>> getChargeBackData() {
		System.out.println("--showChargeBackData");
		List<ChargebackData> chargebackDataList = bankService.getChargeBackData();	
		return ResponseEntity.ok().body(chargebackDataList);
	}
	@GetMapping("/profitPercent")
	public ResponseEntity<List<ProfitData>> getProfitData() {
		System.out.println("--showProfitData");
		List<ProfitData> profitDataList = bankService.getProfitData();	
		return ResponseEntity.ok().body(profitDataList);
	}
	@GetMapping("/profitPercent/{profitPercent}")
	public ResponseEntity<ProfitData> getProfitDataCorrespond(@PathVariable("profitPercent") Long profitPercent) {
		System.out.println("--showProfitData--percentageBasis");
		ProfitData profitData = bankService.getProfitData(profitPercent);	
		return ResponseEntity.ok().body(profitData);
	}
	@GetMapping("/chargeBackPercent/{chargebackPercent}")
	public ResponseEntity<ChargebackData> getChargeBackDataCorrespond(@PathVariable("chargebackPercent") Long chargebackPercent) {
		System.out.println("--showProfitData--percentageBasis");
		ChargebackData chargebackData = bankService.getChargeBack(chargebackPercent);	
		return ResponseEntity.ok().body(chargebackData);
	}
}
