package com.cts.chargeback.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cts.chargeback.entity.StockPrice;

@Repository
public class StockPriceDaoImpl implements StockPriceDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<StockPrice> getStockPriceList(){
		@SuppressWarnings("unchecked")
		TypedQuery<StockPrice> query = sessionFactory.getCurrentSession().createQuery("from StockPrice");
		return query.getResultList();
	}

	@Override
	public StockPrice getStockPrice(String stockName) {
		@SuppressWarnings("unchecked")
		TypedQuery<StockPrice> query = sessionFactory.getCurrentSession().createQuery("from StockPrice where NAME=:stkName");
		query.setParameter("stkName",stockName);
		return query.getResultList().get(0);

	}

}
