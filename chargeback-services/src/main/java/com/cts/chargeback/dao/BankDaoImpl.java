package com.cts.chargeback.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cts.chargeback.entity.ChargebackData;
import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.ProfitData;
import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.FundUser;

@Repository
public class BankDaoImpl implements BankDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Transaction> getTransactions(Long userId,String buyOrSell) {
		@SuppressWarnings("unchecked")
		TypedQuery<Transaction> query = sessionFactory.getCurrentSession().createQuery("from Transaction where USER_ID=:userId AND TRANSACTION_TYPE=:buyOrSell");
		query.setParameter("userId", userId);
		query.setParameter("buyOrSell", buyOrSell);
		return query.getResultList();
	}
	@Override
	public void generateTransaction(Transaction transaction) {
		@SuppressWarnings("unchecked")
		TypedQuery<User> query = sessionFactory.getCurrentSession().createQuery("from User where USER_ID=:userId");
		query.setParameter("userId", transaction.getUserId());
		User user = query.getResultList().get(0);
		user.setBalance(transaction.getBalanceInDollar());
		sessionFactory.getCurrentSession().save(user);
		sessionFactory.getCurrentSession().save(transaction);
	}
	@Override
	public void createFund(Fund fund) {
		sessionFactory.getCurrentSession().save(fund);
	}
	@Override
	public Fund enrollUser(FundUser fundUser) {
		Fund fund = sessionFactory.getCurrentSession().get(Fund.class,fundUser.getFundId());
		User user= sessionFactory.getCurrentSession().get(User.class,fundUser.getUserId());
		user.setRole("FundUser");
		fund.getUsers().add(user);
		fund.setFundBalance(fund.getFundBalance()+user.getBalance());
		sessionFactory.getCurrentSession().save(fund);
		return fund;
	}
	@Override
	public List<User> showFundUsers(Long fundId) {
		@SuppressWarnings("unchecked")
		TypedQuery<Fund> query = sessionFactory.getCurrentSession().createQuery("from Fund where FUND_ID=:fundId");
		query.setParameter("fundId", fundId);
		List<Fund> fund=query.getResultList();
		List<User> users = new ArrayList<>();
		if(fund.size()!=0){
			users = fund.get(0).getUsers();
		}
		return users;
	}
	@Override
	public List<Fund> getFundList() {
		@SuppressWarnings("unchecked")
		TypedQuery<Fund> query = sessionFactory.getCurrentSession().createQuery("from Fund");
		return query.getResultList();
	}

	@Override
	public List<Transaction> getTransactionsNotSold(Long userId) {
		//@SuppressWarnings("unchecked")
		//TypedQuery<Long> queryTwo =sessionFactory.getCurrentSession().createQuery("select t.boughtId from Transaction t where  t.boughtId IS NOT NULL");
		//List<Long> boughtIds = queryTwo.getResultList();
		@SuppressWarnings("unchecked")
		TypedQuery<Transaction> query = sessionFactory.getCurrentSession().createQuery("from Transaction where USER_ID=:userId AND TRANSACTION_TYPE=:buyOrSell AND TRANSACTION_ID NOT IN (select t.boughtId from Transaction t where  t.boughtId IS NOT NULL)");
		query.setParameter("userId", userId);
		query.setParameter("buyOrSell", "BUY");
		//query.setParameter("boughtIds", boughtIds);
		return query.getResultList();
	}
	@Override
	public List<ChargebackData> getChargebackData() {
		@SuppressWarnings("unchecked")
		TypedQuery<ChargebackData> query = sessionFactory.getCurrentSession().createQuery("from ChargebackData");
		return query.getResultList();
	}
	@Override
	public List<ProfitData> getProfitData() {
		@SuppressWarnings("unchecked")
		TypedQuery<ProfitData> query = sessionFactory.getCurrentSession().createQuery("from ProfitData");
		return query.getResultList();
	}
	@Override
	public ProfitData getProfitData(Long profitPercent) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		TypedQuery<ProfitData> query = sessionFactory.getCurrentSession().createQuery("from ProfitData where lower <= :profitPercent AND upper >= :profitPercent");
		query.setParameter("profitPercent", profitPercent);
		ProfitData pd= query.getResultList().get(0);
		return pd;
	}
	@Override
	public ChargebackData getChargeBack(Long chargeBackPercent) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		TypedQuery<ChargebackData> query = sessionFactory.getCurrentSession().createQuery("from ChargebackData where lower <= :chargeBackPercent AND upper >= :chargeBackPercent");
		query.setParameter("chargeBackPercent", chargeBackPercent);
		ChargebackData cd= query.getResultList().get(0);
		return cd;
	}
}
