package com.cts.chargeback.dao;

import java.util.List;

import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.Login;

public interface UserDao {
	User add(User user);

	List<User> listUsers();
	
	User validateUser(Login login);

	List<User> listIndividualUsers();

	User showUser(Integer userId);
}
