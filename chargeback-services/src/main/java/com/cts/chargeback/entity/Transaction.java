package com.cts.chargeback.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TRANSACTION")
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TRANSACTION_ID")
	private Long transactionId;
	
	@Column(name = "USER_ID")
	private Long userId;
	
	@Column(name = "STOCK_NAME")
	private String stockName;
	
	@Column(name = "STOCK_PRICE")
	private Double stockPrice;
	
	@Column(name = "QUANTITY")
	private Integer quantity;
	
	@Column(name = "CHARGE_BACK")
	private Double chargeBack;
	
	@Column(name = "CURRENCY")
	private String currency;
	
	@Column(name = "BALANCE_IN_CURRENCY")
	private Double balanceInCurrency;
	
	@Column(name = "BALANCE_IN_DOLLAR")
	private Double balanceInDollar;
	
	@Column(name = "TRANSACTION_TYPE")
	private String transactionType;//buy/sell
	
	@Column(name = "DATE_TIME")
	private String dateTime;

	@Column(name = "BOUGHT_ID")
	private Long boughtId;
	
	public Long getTransactionId() {
		return transactionId;
	}

	public Long getBoughtId() {
		return boughtId;
	}

	public void setBoughtId(Long boughtId) {
		this.boughtId = boughtId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getChargeBack() {
		return chargeBack;
	}

	public void setChargeBack(Double chargeBack) {
		this.chargeBack = chargeBack;
	}

	public Double getBalanceInCurrency() {
		return balanceInCurrency;
	}

	public void setBalanceInCurrency(Double balanceInCurrency) {
		this.balanceInCurrency = balanceInCurrency;
	}

	public Double getBalanceInDollar() {
		return balanceInDollar;
	}

	public void setBalanceInDollar(Double balanceInDollar) {
		this.balanceInDollar = balanceInDollar;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
		
}
