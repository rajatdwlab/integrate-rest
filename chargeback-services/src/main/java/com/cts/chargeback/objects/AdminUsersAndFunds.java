package com.cts.chargeback.objects;

import java.util.List;

import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.User;

public class AdminUsersAndFunds {
	
	private User user;
	private List<User> individualUsers;
	private List<Fund> funds;
	public AdminUsersAndFunds(User user, List<User> individualUsers,List<Fund> funds) {
		super();
		this.user = user;
		this.individualUsers = individualUsers;
		this.funds=funds;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<User> getIndividualUsers() {
		return individualUsers;
	}
	public void setIndividualUsers(List<User> individualUsers) {
		this.individualUsers = individualUsers;
	}
	public List<Fund> getFunds() {
		return funds;
	}
	public void setFunds(List<Fund> funds) {
		this.funds = funds;
	}

	
}
