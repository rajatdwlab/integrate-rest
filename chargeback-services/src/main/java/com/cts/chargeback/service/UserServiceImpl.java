package com.cts.chargeback.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.Login;
import com.cts.chargeback.dao.UserDao;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Transactional
	@Override
	public User add(User user) {
		return userDao.add(user);
	}

	@Transactional(readOnly = true)
	@Override
	public List<User> listUsers() {
		return userDao.listUsers();
	}
	@Transactional(readOnly = true)
	@Override
	public List<User> listIndividualUsers() {
		return userDao.listIndividualUsers();
	}
	@Transactional
	@Override
	public User validateUser(Login login) {
		return userDao.validateUser(login);
	}
	@Transactional
	@Override
	public User showUser(Integer userId) {
		// TODO Auto-generated method stub
		return userDao.showUser(userId);
	}
}
