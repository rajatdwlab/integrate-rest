package com.cts.chargeback.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cts.chargeback.dao.BankDao;
import com.cts.chargeback.dao.StockPriceDao;
import com.cts.chargeback.entity.ChargebackData;
import com.cts.chargeback.entity.Fund;
import com.cts.chargeback.entity.ProfitData;
import com.cts.chargeback.entity.StockPrice;
import com.cts.chargeback.entity.Transaction;
import com.cts.chargeback.entity.User;
import com.cts.chargeback.objects.FundUser;
@Service("bankService")
public class BankServiceImpl implements BankService{
	
	@Autowired
	private BankDao bankDao;
	@Autowired
	private StockPriceDao stockPriceDao;
	@Transactional
	@Override
	public List<Transaction> getTransactions(Long userId,String buyOrSell) {
		return bankDao.getTransactions(userId,buyOrSell);
	}
	
	@Transactional
	@Override
	public void generateTransaction(Transaction transaction) {
		bankDao.generateTransaction(transaction);
	}
	
	@Transactional
	@Override
	public void createFund(Fund fund) {
		bankDao.createFund(fund);
		
	}
	
	@Transactional
	@Override
	public Fund enrollUser(FundUser fundUser) {
		Fund fund = bankDao.enrollUser(fundUser);
		return fund;
	}
	@Transactional
	@Override
	public List<User> showFundUsers(Long fundId) {
		return bankDao.showFundUsers(fundId);
	}
	@Transactional
	@Override
	public List<StockPrice> showStockPrice() {
		
		return stockPriceDao.getStockPriceList();
	}
	@Transactional
	@Override
	public List<Fund> getFundList() {
		// TODO Auto-generated method stub
		return bankDao.getFundList();
	}

	@Transactional
	@Override
	public List<Transaction> getTransactionsNotSold(Long userId) {
		// TODO Auto-generated method stub
		return bankDao.getTransactionsNotSold(userId);
	}
	@Transactional
	@Override
	public StockPrice showStockPrice(String stockName) {
		// TODO Auto-generated method stub
		return stockPriceDao.getStockPrice(stockName);
	}
	@Transactional
	@Override
	public List<ChargebackData> getChargeBackData() {
		// TODO Auto-generated method stub
		return bankDao.getChargebackData();
	}
	@Transactional
	@Override
	public List<ProfitData> getProfitData() {
		// TODO Auto-generated method stub
		return bankDao.getProfitData();
	}
	@Transactional
	@Override
	public ProfitData getProfitData(Long profitPercent) {
		// TODO Auto-generated method stub
		return bankDao.getProfitData(profitPercent);
	}
	@Transactional
	@Override
	public ChargebackData getChargeBack(Long chargebackPercent) {
		// TODO Auto-generated method stub
		return bankDao.getChargeBack(chargebackPercent);
	}
}
