package com.cts.chargeback.service;

import java.util.List;

import com.cts.chargeback.objects.Login;
import com.cts.chargeback.entity.User;

public interface UserService {
	User add(User user);
    List<User> listUsers();
    User validateUser(Login login);
    public List<User> listIndividualUsers();
	User showUser(Integer userId);
}

