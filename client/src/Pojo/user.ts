export class User {
    constructor(public userId:number,public password:string,public firstName:string,public lastName:string,
        public role:string,public age:number, public gender:string,
        public contactNo:string,public emailId:string,public address:string,
        public city:string,public zipCode:string,public balance: number){}
 }
 