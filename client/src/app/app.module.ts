import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { UserService } from './user.service';
import { HttpModule } from '@angular/http';
import { DisplayComponent } from './display/display.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminDisplayComponent } from './admin-display/admin-display.component';
import { IndividualTradeComponent } from './individual-trade/individual-trade.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    HomeComponent,
    DisplayComponent,
    AdminloginComponent,
    AdminDisplayComponent,
    IndividualTradeComponent
  ],
  imports: [
    
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'register',component:RegisterComponent},
      {path:'users',component:UserComponent},
      {path:'login',component:LoginComponent},
      {path:'login/:userId',component:LoginComponent},
      {path:'',component:HomeComponent},
      {path:'home',component:HomeComponent},
      {path:'display/:userId/:password',component:DisplayComponent},
      {path:'displayAdmin/:userId/:password',component:AdminDisplayComponent},
      {path:'adminlogin',component:AdminloginComponent},
      {path:'individual-trade/:userId',component:IndividualTradeComponent}
    ])    
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
