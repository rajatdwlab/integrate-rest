import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Login } from '../../Pojo/login';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { Transaction } from '../../Pojo/transaction';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public userId:number;
  public password:string;
  public message:string='';
  constructor(private userservice:UserService,private route: ActivatedRoute) { }

  ngOnInit() {
    const id=+this.route.snapshot.paramMap.get('userId');
    console.log(id);
    if(id!=0)
      this.message="Registered  Successfully !!! your userId is "+id;
    else{
      this.message="";
    }
  }

}
